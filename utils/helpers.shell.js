const { exec } = require("child_process");
const { defer } = require("utils-lib-js");
// shell命令封装
exports.shell = (__shell, showLog = true, opts = {}) => {
  const { resolve, reject, promise } = defer();
  exec(__shell, opts, (err, str, errStr) => {
    const __err = err ?? errStr;
    showLog && console.info(`>>>>> ${__shell}: `, __err || str);
    if (__err) reject(__err);
    else resolve(str);
  });
  return promise;
};
