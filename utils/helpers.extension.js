const { shell } = require("./helpers.shell");
const { catchAwait } = require("utils-lib-js");
// 命令拓展
exports.othersCommand = async function (opts) {
  const { tag, others } = opts;
  if (!!others.publish) {
    change2Npm();
    return publishNpm(tag);
  }
};
// 发布到npm
async function publishNpm(tag) {
  const [err] = await catchAwait(shell(`npm publish`, false));
  if (err) return err;
  console.log(`publish ${tag} to npm success`);
}

// 切换镜像到npm
async function change2Npm() {
  const [err] = await catchAwait(shell(`nrm use npm`, false));
  if (err) return err;
}

// 运行构建命令
exports.buildProject = async function ({ tag }) {
  const [err] = await catchAwait(shell(`npm run build`, false));
  if (err) return err;
  console.log(`build ${tag} success`);
};
