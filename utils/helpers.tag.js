const { git } = require("./helpers.git");
// git-tag
exports.gitTag = async ({ tag, message }) => {
  console.log(`git-tag-start-------------`, tag);
  message && console.log(`git-tag-message-------------`, message);
  const err = await git.hasTag(tag);
  if (err) return err;
  const err2 = await git.tag(tag, message);
  if (err2) return err2;
  const err3 = await git.push(tag);
  if (err3) return err3;
  console.log(`git-tag-success-------------`, tag);
};
