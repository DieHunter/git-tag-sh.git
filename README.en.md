# git-tag-sh

#### Introduction

Execute git packaging operation for the current project

#### Installation Tutorial

1. Global installation: npm install git-tag-sh - g
2. Installation in the project: npm install git-tag-sh

#### Instructions for use

1. Version number: git-tag-sh -V
2. Help: git-tag-sh -h
3. Current directory version number: git-tag-sh-cv output: 1.0.0
4. Current version tag: git-tag-sh output: 1.0.0
5. Print the official tag (add the tag prefix) for the current version: git-tag-sh -p [string] Output: v1.0.0
6. The current version is labeled with the tag suffix test: git-tag-sh -s test output: 1.0.0-test
7. The current version uses the tag suffix alpha (beta, release): git-tag-sh -a (-b, -r) output: 1.0.0-alpha (beta, release)
8. Submit tag message: git-tag-sh -m [string]
9. Publish to npm: git tag sh -pub
10. Run the npm build command: git tag sh -bu

#### Participation contribution

1. Fork warehouse
2. Star warehouse
